<?php

return [
    'id' => 'tests',
    'basePath' => dirname(__DIR__),
    'bootstrap' => [\JzWebstudio\Yii2PageMetaData\Bootstrap::class],
    'vendorPath' => dirname(__DIR__) . '/vendor',
    'components' => [
        'db' => [
            'class' => yii\db\Connection::class,
            //'dsn' => 'mysql:host=db;dbname=test_module',
            'dsn' => 'mysql:host=db;dbname=test_module',
            'username' => 'root',
            'password' => 'rootpassword'
        ],
    ],
    'modules' => [
        'metatag' => \JzWebstudio\Yii2PageMetaData\Module::class
    ]
];