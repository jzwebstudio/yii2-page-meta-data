<?php
namespace Repository;

use ProxyManager\Factory\LazyLoadingValueHolderFactory;
use JzWebstudio\Yii2PageMetaData\Repositories\Yii2DaoPageRepository;

class Yii2DaoPageRepositoryTest extends \RobotE13\PageMeta\Tests\PageRepositoryAbstractTest
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    /**
     * @var \RobotE13\PageMeta\Repositories\PageRepository
     */
    protected $repository;

    protected function _before()
    {
        parent::_before();
        $this->repository = new Yii2DaoPageRepository(new LazyLoadingValueHolderFactory());
    }

    protected function _after()
    {
    }

}