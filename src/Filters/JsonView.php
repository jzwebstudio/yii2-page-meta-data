<?php

/**
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JzWebstudio\Yii2PageMetaData\Filters;

use Yii;
use yii\web\Response;

/**
 * Description of JsonView
 *
 * @author Evgenii Dudal <wolfstrace@gmail.com>
 */
class JsonView extends \yii\base\Component
{
    public function render($view, $params = [], $context = null)
    {
        $context->layout = false;
        $response = Yii::$app->getResponse();
        $response->format = Response::FORMAT_JSON;
        return $params;
    }


}
