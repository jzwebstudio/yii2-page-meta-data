<?php

/**
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JzWebstudio\Yii2PageMetaData\Filters;

use Yii;

/**
 * Description of ContentNegotiator
 *
 * @author Evgenii Dudal <wolfstrace@gmail.com>
 */
class ContentNegotiator extends \yii\base\ActionFilter
{
    public function beforeAction($action)
    {
        $this->negotiate($action);
        return true;
    }

    public function afterAction($action, $result)
    {
        if(!$action->controller->view instanceof JsonView)
        {
            return $result;
        }

        $serializer = Yii::createObject([
            'class' => \yii\rest\Serializer::class,
            'collectionEnvelope' => 'items'
        ]);
        return array_map(function($item) use ($serializer) {
            return $serializer->serialize($item);
        }, $result);
    }

    /**
     *
     * @param \yii\base\Action $action
     */
    public function negotiate($action)
    {
        $request = Yii::$app->request;
        if(stripos($request->headers->get('Accept'), 'application/json') !== false)
        {
            $action->controller->setView(new JsonView());
        }
    }
}
