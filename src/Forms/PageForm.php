<?php
/**
 * This file is part of the yii2-page-meta-data.
 *
 * Copyright 2020 Green Wave Palace Ltd. <jzwebstudio@gmail.com>.
 *
 * This source file is subject to the Commercial license that is bundled
 * with this source code in the file LICENSE.
 * @package yii2-page-meta-data
 */

namespace JzWebstudio\Yii2PageMetaData\Forms;

use elisdn\compositeForm\CompositeForm;
use RobotE13\PageMeta\Entities\HTMLBlock\HtmlBlock;
use RobotE13\PageMeta\Entities\MetaTag\MetaTag;
use RobotE13\PageMeta\Services\Page\PageDTO;

/**
 * PageCreateForm
 *
 * @author Sergey Bakhtin <sergey.a.bahtin@gmail.com>
 */
class PageForm extends CompositeForm
{
    public $route;
    public $canonical;

    public function __construct($countRouteParams = 1, $countMetaTags = 1, $countHtmlBlocks = 1, $config = [])
    {
        $this->routeParams = array_map(function () {
            return new RouteParamForm();
        }, range(1, !$countRouteParams ? 1 : $countRouteParams));

        $this->metaTags = array_map(function () {
            return new MetaTagForm();
        }, range(1, !$countMetaTags ? 1 : $countMetaTags));

        $this->htmlBlocks = array_map(function () {
            return new HtmlBlockForm();
        }, range(1, !$countHtmlBlocks ? 1 : $countHtmlBlocks));

        parent::__construct($config);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['route', 'required'],
            ['canonical', 'default', 'value' => ''],
        ];
    }

    protected function internalForms(): array
    {
        return ['routeParams', 'metaTags', 'htmlBlocks'];
    }

    public function getDto(): PageDTO
    {
        $dto = new PageDTO();
        $dto->route = $this->route;
        $dto->canonical = $this->canonical;
        $dto->routeParams = [];
        $dto->metaTags = [];
        $dto->htmlBlocks = [];

        foreach ($this->routeParams as $routeParam) {
            $dto->routeParams[$routeParam['key']] = $routeParam['value'];
        }

        foreach ($this->metaTags as $metaTag) {
            $attributes = [];
            foreach ($metaTag['attributes'] as $attribute) {
                $attributes[$attribute['key']] = $attribute['value'];
            }

            $dto->metaTags[] = new MetaTag(
                $metaTag['name'],
                $metaTag['content'],
                $attributes
            );
        }

        foreach ($this->htmlBlocks as $htmlBlock) {
            $dto->htmlBlocks[] = new HtmlBlock($htmlBlock['name'], $htmlBlock['content']);
        }

        return $dto;
    }

    public function formName()
    {
        return 'PageForm';
    }
}