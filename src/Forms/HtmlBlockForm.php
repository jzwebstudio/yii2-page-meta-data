<?php

namespace JzWebstudio\Yii2PageMetaData\Forms;

use yii\base\Model;
use Yii;

class HtmlBlockForm extends Model
{
    public $name;
    public $content;

    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name', 'content'], 'string'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => Yii::t('jzwebstudio/pagemeta', 'Name'),
            'content' => Yii::t('jzwebstudio/pagemeta', 'Content'),
        ];
    }
}
