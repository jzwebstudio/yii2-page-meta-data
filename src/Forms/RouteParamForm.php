<?php
/**
 * This file is part of the yii2-page-meta-data.
 *
 * Copyright 2020 Green Wave Palace Ltd. <jzwebstudio@gmail.com>.
 *
 * This source file is subject to the Commercial license that is bundled
 * with this source code in the file LICENSE.
 * @package yii2-page-meta-data
 */

namespace JzWebstudio\Yii2PageMetaData\Forms;

use Yii;
use yii\base\Model;

/**
 * RouteParamsForm
 *
 * @author Sergey Bakhtin <sergey.a.bahtin@gmail.com>
 */
class RouteParamForm extends Model
{
    public $key;
    public $value;

    public function rules()
    {
        return [
            [['key', 'value'], 'string'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'key' => Yii::t('jzwebstudio/pagemeta', 'Key'),
            'value' => Yii::t('jzwebstudio/pagemeta', 'Value'),
        ];
    }
}