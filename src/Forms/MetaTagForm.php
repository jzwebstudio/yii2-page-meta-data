<?php
/**
 * This file is part of the yii2-page-meta-data.
 *
 * Copyright 2020 Green Wave Palace Ltd. <jzwebstudio@gmail.com>.
 *
 * This source file is subject to the Commercial license that is bundled
 * with this source code in the file LICENSE.
 * @package yii2-page-meta-data
 */

namespace JzWebstudio\Yii2PageMetaData\Forms;

use Yii;
use yii\base\BaseObject;
use yii\base\Model;
use elisdn\compositeForm\CompositeForm;

/**
 * MetaTagForm
 *
 * @author Sergey Bakhtin <sergey.a.bahtin@gmail.com>
 */
class MetaTagForm extends Model
{
    public $name;
    public $content;
    public array $attributes;

    public function __construct($countAttributes = 1, $config = array())
    {
        $this->attributes = array_map(function ($index) {
            return ['key' => '', 'value' => ''];
        }, range(1, !$countAttributes ? 1 : $countAttributes));

        parent::__construct($config);
    }

    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name', 'content'], 'string'],
            ['attributes', function ($attribute, $params) {
                if(!is_array($this->$attribute)){
                    $this->addError($attribute, 'Attrubutes must be array');
                }
            }],
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => Yii::t('jzwebstudio/pagemeta', 'Name'),
            'content' => Yii::t('jzwebstudio/pagemeta', 'Content'),
        ];
    }
}