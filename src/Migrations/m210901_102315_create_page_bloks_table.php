<?php

namespace JzWebstudio\Yii2PageMetaData\Migrations;

/**
 * Handles the creation of table `{{%page_bloks}}`.
 */
class m210901_102315_create_page_bloks_table extends Migration
{

    protected $table = '{{%page_blocks}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable($this->table, [
            'page_uuid' => "BINARY(16) NOT NULL",
            'name' => $this->string()->notNull(),
            'content' => $this->text()->notNull()
        ], $this->tableOptions);

        $this->addPrimaryKey('', $this->table, ['page_uuid', 'name']);
        $this->createIndex('fk_page_pageblocks_idx', $this->table, ['page_uuid']);
        $this->addForeignKey('fk_pageblocks_page', $this->table, 'page_uuid',
                "{{%metatag_pages}}", 'uuid', $this->cascade, $this->cascade);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable($this->table);
    }

}
