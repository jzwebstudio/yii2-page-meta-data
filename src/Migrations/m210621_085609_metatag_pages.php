<?php
/**
 * This file is part of the yii2-page-meta-data.
 *
 * Copyright 2020 Green Wave Palace Ltd. <jzwebstudio@gmail.com>.
 *
 * This source file is subject to the Commercial license that is bundled
 * with this source code in the file LICENSE.
 * @package yii2-page-meta-data
 */

namespace JzWebstudio\Yii2PageMetaData\Migrations;

use yii\db\Migration;

/**
 * Class m210621_085609_metatag_pages
 * @author Sergey Bakhtin <sergey.a.bahtin@gmail.com>
 */
class m210621_085609_metatag_pages extends Migration
{
    protected $pageTable = "{{%metatag_pages}}";

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable($this->pageTable, [
            'uuid' => "BINARY(16) NOT NULL",
            'route' => $this->string()->notNull(),
            'route_params' => $this->json()->notNull(),
            'metatags' => $this->json()->notNull()->defaultValue(json_encode([])),
            'canonical' => $this->string()->notNull(),
            'created_at' => $this->timestamp()->notNull(),
        ]);
        $this->addPrimaryKey('', $this->pageTable, 'uuid');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable($this->pageTable);
    }
}
