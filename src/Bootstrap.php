<?php

/**
 * This file is part of the yii2-page-meta-data.
 *
 * Copyright 2020 Green Wave Palace Ltd. <jzwebstudio@gmail.com>.
 *
 * This source file is subject to the Commercial license that is bundled
 * with this source code in the file LICENSE.
 * @package yii2-page-meta-data
 */

namespace JzWebstudio\Yii2PageMetaData;

use yii\helpers\ArrayHelper;

/**
 * Description of Bootstrap
 *
 * @author Sergey Bakhtin <sergey.a.bahtin@gmail.com>
 */
class Bootstrap implements \yii\base\BootstrapInterface
{

    /**
     *
     * @param \yii\base\Application $app
     */
    public function bootstrap($app)
    {
        if($app instanceof \yii\console\Application)
        {
            // добавление пути к миграциям модуля в Yii контроллер миграций
            $app->controllerMap = ArrayHelper::merge($app->controllerMap, [
                'migrate' => [
                    'class' => \yii\console\controllers\MigrateController::class,
                    'migrationNamespaces' => ['JzWebstudio\Yii2PageMetaData\Migrations']
                ]
            ]);
        } else
        {
            $app->urlManager->addRules([
                [
                    'class' => \yii\web\GroupUrlRule::class,
                    'prefix' => 'metatag',
                    'rules' => [
                        'GET ' => 'page/index',
                        'page/create' => 'page/create',
                        'page/update/<uid:[\w\-]+>' => 'page/update',
                    ]
                ]
            ],false);
        }
        $app->i18n->translations['jzwebstudio/pagemeta'] = [
            'class' => \yii\i18n\PhpMessageSource::class,
            'sourceLanguage' => 'en',
            'forceTranslation' => true,
            'basePath' => '@JzWebstudio/Yii2PageMetaData/messages',
            'fileMap' => [
                'page-meta' => 'page-meta.php'
            ]
        ];
    }

}