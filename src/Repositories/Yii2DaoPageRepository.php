<?php

/**
 * This file is part of the yii2-page-meta-data.
 *
 * Copyright 2020 Green Wave Palace Ltd. <jzwebstudio@gmail.com>.
 *
 * This source file is subject to the Commercial license that is bundled
 * with this source code in the file LICENSE.
 * @package yii2-page-meta-data
 */

namespace JzWebstudio\Yii2PageMetaData\Repositories;

use DomainException;
use RobotE13\PageMeta\Entities\MetaTag\MetaTag;
use RobotE13\PageMeta\Entities\MetaTag\MetaTagsCollection;
use RobotE13\PageMeta\Entities\HTMLBlock\HtmlBlock;
use RobotE13\PageMeta\Entities\HTMLBlock\HtmlBlocksCollection;
use RobotE13\PageMeta\Repositories\NotFoundException;
use samdark\hydrator\Hydrator;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use yii\db\Connection;
use ProxyManager\Proxy\LazyLoadingInterface;
use ProxyManager\Factory\LazyLoadingValueHolderFactory;
use RobotE13\DDD\Entities\Uuid\Id;
use RobotE13\PageMeta\Entities\Page;
use RobotE13\PageMeta\Repositories\PageRepository;
use JzWebstudio\Yii2PageMetaData\Adapters\MetaTagsSerializer;
use yii\rest\Serializer;

/**
 * Yii2DaoPageRepository
 *
 * @author Sergey Bakhtin <sergey.a.bahtin@gmail.com>
 */
class Yii2DaoPageRepository implements PageRepository
{

    /**
     * @var Connection
     */
    private $db;

    /**
     * @var LazyLoadingValueHolderFactory
     */
    private $lazyFactory;
    protected $pageTable = "{{%metatag_pages}}";
    protected $htmlBlocksTable = "{{%page_blocks}}";

    public function __construct(LazyLoadingValueHolderFactory $lazyFactory, string $dbConnection = 'db')
    {
        $this->lazyFactory = $lazyFactory;
        $this->db = Yii::$app->get($dbConnection);
    }

    /**
     * {@inheritdoc}
     */
    public function add(Page $page): void
    {
        $pageExist = $this->findByCondition([
            'route' => $page->getRoute(),
            'route_params' => json_encode($page->getRouteParams())
        ]);

        if ($pageExist)
        {
            throw new DomainException('Page ' . $page->getRoute() . ' exist!');
        }

        $this->db->createCommand()
                ->insert($this->pageTable, $this->extract($page))
                ->execute();
        $this->updateBlocks($page);
    }

    /**
     * {@inheritdoc}
     */
    public function findById(Id $id): Page
    {
        $page = $this->findByCondition(['uuid' => $id->getBytes()]);

        if (!$page)
        {
            throw new NotFoundException('Page not found.');
        }

        return $this->populate($page);
    }

    /**
     * {@inheritdoc}
     */
    public function findByRoute(string $route): Page
    {
        $page = $this->findByCondition(['route' => $route]);

        if (!$page)
        {
            throw new NotFoundException('Page not found.');
        }

        return $this->populate($page);
    }

    /**
     * {@inheritdoc}
     */
    public function findByRouteWithParams(string $route, array $routeParams): Page
    {
        $pages = (new Query())
            ->from($this->pageTable)
            ->where(['route' => $route])
            ->all($this->db);

        $foundPage = null;

        foreach ($pages as $page) {
            $pageRouteParams = json_decode($page['route_params'], true);
            $matches = array_intersect_assoc($routeParams, $pageRouteParams);
            if (count($matches) === count($pageRouteParams)) {
                $foundPage = $page;
                break;
            }
        }

        if (!$foundPage) {
            throw new NotFoundException('Page not found.');
        }

        return $this->populate($foundPage);
    }

    /**
     * {@inheritdoc}
     */
    public function findAll($page = 1, $perPage = 0): array
    {
        $query = (new Query())
                ->from($this->pageTable);
        $provider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => $perPage,
                'page' => $page - 1,
                'route' => 'metatag'
            ],
            'sort' => [
                'attributes' => [
                    'created_at',
                ],
                'defaultOrder' => [
                    'created_at' => SORT_DESC,
                ]
            ],
        ]);

        $provider->setModels(array_map(fn($page) => $this->populate($page), $provider->getModels()));

        $serializer = new Serializer();
        $serializer->collectionEnvelope = 'items';

        return $serializer->serialize($provider);
    }

    /**
     * {@inheritdoc}
     */
    public function remove($id): void
    {
        $uid = Id::fromString($id->getString())->getBytes();
        if ((new \yii\db\Query())
                        ->from($this->pageTable)
                        ->where(['uuid' => $uid])
                        ->exists())
        {
            $this->db->createCommand()->delete($this->pageTable, ['uuid' => $uid])->execute();
        } else
        {
            throw new NotFoundException('Page not found.');
        }
    }

    /**
     * {@inheritdoc}
     */
    public function update(Page $page): void
    {
        $this->db->createCommand()
                ->update($this->pageTable, $this->extract($page), ['uuid' => $page->getUid()->getBytes()])
                ->execute();
        $this->updateBlocks($page);
    }

    private function findByCondition($condition)
    {
        return (new Query())
                        ->from($this->pageTable)
                        ->where($condition)
                        ->one($this->db);
    }

    private function extract(Page $page)
    {
        return [
            'uuid' => $page->getUid()->getBytes(),
            'created_at' => $page->getCreatedAt()->format('Y-m-d H:i:s'),
            'route' => $page->getRoute(),
            'route_params' => json_encode($page->getRouteParams()),
            'canonical' => $page->getCanonical(),
            'metatags' => json_encode((new MetaTagsSerializer($page->getMetaTags()))->jsonSerialize()),
        ];
    }

    private function populate($row): Page
    {
        $uid = Id::fromBytes($row['uuid']);
        $row['uuid'] = $uid;
        $row['created_at'] = new \DateTimeImmutable($row['created_at']);
        $row['route_params'] = json_decode($row['route_params'], true);
        $row['metatags'] = new MetaTagsCollection(array_map(function($item) {
                    $item['attributes'] = !empty($item['attributes']) ? json_decode($item['attributes'], true) : [];
                    return (new Hydrator([
                                'name' => 'name',
                                'content' => 'content',
                                'attributes' => 'attributes'
                                    ]))->hydrate($item, MetaTag::class);
                }, json_decode($row['metatags'], true)));

        $row['html_blocks'] = $this->lazyFactory->createProxy(HtmlBlocksCollection::class,
            function (&$target, LazyLoadingInterface $proxy) use ($uid) {
                $blocks = (new Query())->select('*')
                        ->from($this->htmlBlocksTable)
                        ->andWhere(['page_uuid' => $uid->getBytes()])
                        ->all($this->db);
                $target = new HtmlBlocksCollection(array_map(
                        fn($block) => new HtmlBlock($block['name'], $block['content']),
                        $blocks
                    )
                );
                $proxy->setProxyInitializer(null);
            }
        );

        $hydrator = new Hydrator([
            'uuid' => 'uid',
            'created_at' => 'createdAt',
            'route' => 'route',
            'route_params' => 'routeParams',
            'metatags' => 'metaTags',
            'html_blocks' => 'blocks',
            'canonical' => 'canonical',
        ]);

        return $hydrator->hydrate($row, Page::class);
    }

    /**
     *
     * @param Page $page
     */
    private function updateBlocks(Page $page)
    {
        $pageBlocksObject = (new Hydrator(['blocks' => 'blocks']))->extract($page)['blocks'];
        if ($pageBlocksObject instanceof LazyLoadingInterface && !$pageBlocksObject->isProxyInitialized())
        {
            return;
        }

        $blocks = array_map(function ($item) use ($page) {
            /* @var $item \RobotE13\PageMeta\Entities\HTMLBlock\HtmlBlock */
            return[
                'name' => $item->getName(),
                'content' => $item->getContent(),
                'page_uuid' => $page->getUid()->getBytes()
            ];
        }, $page->getHtmlBlocks()->toArray());

        $transaction = $this->db->beginTransaction();
        try {
            $this->db->createCommand()
                    ->delete($this->htmlBlocksTable, ['page_uuid' => $page->getUid()->getBytes()])
                    ->execute();
            if (!empty($blocks))
            {
                $this->db->createCommand()
                        ->batchInsert($this->htmlBlocksTable, array_keys(reset($blocks)), $blocks)
                        ->execute();
            }
            $transaction->commit();
        } catch (\Throwable $exc) {
            $transaction->rollBack();
            throw $exc;
        }
    }

}
