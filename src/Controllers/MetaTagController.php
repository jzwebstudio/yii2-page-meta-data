<?php

/**
 * This file is part of the yii2-page-meta-data.
 *
 * Copyright 2020 Green Wave Palace Ltd. <jzwebstudio@gmail.com>.
 *
 * This source file is subject to the Commercial license that is bundled
 * with this source code in the file LICENSE.
 * @package yii2-page-meta-data
 */

namespace JzWebstudio\Yii2PageMetaData\Controllers;

use Yii;

/**
 * yii2-page-meta-data module definition class
 *
 * @author Sergey Bakhtin <sergey.a.bahtin@gmail.com>
 */
class MetaTagController extends \yii\web\Controller
{

    public function actionCreate()
    {
        return $this->render('index');
    }

    public function actionUpdate()
    {
        return $this->render('view');
    }

}