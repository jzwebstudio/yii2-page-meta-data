<?php
/**
 * This file is part of the yii2-page-meta-data.
 *
 * Copyright 2020 Green Wave Palace Ltd. <jzwebstudio@gmail.com>.
 *
 * This source file is subject to the Commercial license that is bundled
 * with this source code in the file LICENSE.
 * @package yii2-page-meta-data
 */

namespace JzWebstudio\Yii2PageMetaData\Controllers;

use Yii;
use yii\helpers\Url;
use RobotE13\DDD\Entities\Uuid\Id;
use RobotE13\PageMeta\Entities\Page;
use RobotE13\PageMeta\Repositories\NotFoundException;
use RobotE13\PageMeta\Services\Page\PageService;
use JzWebstudio\Yii2PageMetaData\Forms\PageForm;
use yii\web\{
    NotFoundHttpException,
    Response,
    Request
};
use RobotE13\Yii2RenderNegotiator\RenderNegotiator;

/**
 * yii2-page-meta-data module definition class
 *
 * @author Sergey Bakhtin <sergey.a.bahtin@gmail.com>
 */
class PageController extends \yii\web\Controller
{
    /**
     *
     * @var PageService
     */
    private $pageService;

    public function __construct($id, $module, PageService $pageService, $config = array())
    {
        $this->pageService = $pageService;
        parent::__construct($id, $module, $config);
    }

    public function behaviors(): array
    {
        return[
            'negotiator' => [
                'class' => RenderNegotiator::class,
                'formats' => [
                    '*/*' => Response::FORMAT_HTML,
                    'text/html' => Response::FORMAT_HTML,
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
            'verb' => [
                'class' => \yii\filters\VerbFilter::class,
                'actions' => [
                    'update' => ['POST'],
                    'create' => ['POST']
                ]
            ]
        ];
    }

    public function actionIndex()
    {
        $page = Yii::$app->request->get('page', 1);
        $perPage = Yii::$app->request->get('per-page', 10);
        $dataProvider = $this->pageService->getPages()->findAll($page, $perPage);

        return $this->render('index', compact('dataProvider'));
    }

    public function actionCreate()
    {
        $countRouteParams = count(Yii::$app->request->post('RouteParamForm', []));
        $countMetaTags = count(Yii::$app->request->post('MetaTagForm', []));
        $model = new PageForm($countRouteParams, $countMetaTags);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $uid = $this->pageService
                ->create($model->getDto())
                ->getCurrentPage()
                ->getUid();

            return $this->redirect(['view', 'uid' => $uid->getString()]);
        }

        return $this->render('create', compact('model'));
    }

    public function actionUpdate()
    {
        //@TODO добавить сервис для работы с данными страницы https://jzwebstudio.myjetbrains.com/youtrack/issue/DS-15
        $pageForm = Yii::$app->request->post('PageForm', []);
        $routeParamsForm = Yii::$app->request->post('RouteParamForm', []);
        $countRouteParams = count($routeParamsForm);
        $countMetaTags = count(Yii::$app->request->post('MetaTagForm', []));
        $countHtmlBlocks = count(Yii::$app->request->post('HtmlBlockForm', []));
        $model = new PageForm($countRouteParams, $countMetaTags, $countHtmlBlocks);

        //find page by uid
        $uid = Yii::$app->request->post('uid');
        $page = $this->pageService->getPages()->findById(Id::fromString($uid));

        $route = !empty($pageForm['route']) && is_string($pageForm['route']) ? $pageForm['route'] : $page->getRoute();
        $routeParams = $page->getRouteParams();
        if (is_array($routeParamsForm) && [] !== $routeParamsForm) {
            $routeParams = [];
            foreach ($routeParamsForm as $routeParamFrom) {
                if (!isset($routeParamFrom['key']) || !isset($routeParamFrom['value'])) {
                    continue;
                }
                $routeParams[$routeParamFrom['key']] = $routeParamFrom['value'];
            }
            if ([] === $routeParams) {
                $routeParams = $page->getRouteParams();
            }
        }
        $data = Yii::$app->request->post();
        $data['PageForm']['route'] = $route;
        $data['RouteParamForm'] = array_map(
            fn($routeParamName, $routeParamValue) => ['key' => $routeParamName, 'value' => $routeParamValue],
            array_keys($routeParams),
            $routeParams
        );
        try {
            if ($model->load($data) && $model->validate()) {
                $dto = $model->getDto();

                $this->pageService
                    ->getPages()
                    ->update(
                        new Page(
                            $page->getUid(),
                            $route,
                            $routeParams,
                            $dto->metaTags,
                            $dto->htmlBlocks,
                            $dto->canonical
                        )
                    );
            }
        } catch (NotFoundException $exc) {
            throw new NotFoundHttpException($exc->getMessage());
        }

        return $this->render('view', compact('page'));
    }

    public function actionView($uid)
    {
        try {
            $page = $this->pageService
                ->getPages()
                ->findById(Id::fromString($uid));
        } catch (NotFoundException $exc) {
            throw new NotFoundHttpException($exc->getMessage());
        }

        return $this->render('view', compact('page'));
    }

    public function actionRemove($uid)
    {
        try {
            $this->pageService
                ->getPages()
                ->remove(Id::fromString($uid));
        } catch (NotFoundException $exc) {
            throw new NotFoundHttpException($exc->getMessage());
        }

        return $this->redirect(Url::previous());
    }

}