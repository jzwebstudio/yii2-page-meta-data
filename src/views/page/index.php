<?php

use RobotE13\DDD\Entities\Uuid\Id;
use yii\helpers\Url;
use yii\helpers\Html;
?>
<table class="metatag-page-list" border="1">
<tr>
    <th>Uid</th>
    <th>Created</th>
    <th>Route</th>
    <th>Route parameters</th>
    <th>MetaTags</th>
    <th>Canonical tag</th>
</tr>
<?php $pages = $dataProvider['items'];
     foreach ($pages as $page) { ?>
    <tr>
        <td><?= $page->getUid()->getString() ?></td>
        <td><?= $page->getCreatedAt()->format('d-m-Y H:i:s') ?></td>
        <td><?= $page->getRoute() ?></td>
        <td>
            <?php foreach ($page->getRouteParams() as $key => $param) {
                echo $key.'='.$param.'<br>';
            } ?>
        </td>
        <td>
            <?php foreach ($page->getMetaTags() as $metaTag) {
                echo 'Name:' . $metaTag->getAttributeName().'; '.
                     'Content:'.$metaTag->getContent().'<br>';
            } ?>
        </td>
        <td><?= Html::Encode($page->getCanonical()) ?></td>
    </tr>
<?php } ?>
</table>
<?php print_r($dataProvider['_meta']); ?>
<br> <a href="<?= Url::to(['page/create']) ?>">Create Page</a>
