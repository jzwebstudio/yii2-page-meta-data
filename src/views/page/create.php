<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$form = ActiveForm::begin() ?>
<h2>Page settings</h2>
<?= $form->field($model, 'route')->textInput() ?>

<?php foreach ($model->routeParams as $i => $routeParamForm): ?>
    <?= 'Route parameter #'.($i+1) ?>
    <?= $form->field($routeParamForm, '[' . $i . ']key')->textInput() ?>
    <?= $form->field($routeParamForm, '[' . $i . ']value')->textInput() ?>
<?php endforeach; ?>

<h2>Meta tags</h2>
<?php foreach ($model->metaTags as $i => $metaTagForm): ?>
    <?= 'Meta tag #'.($i+1) ?>
    <?= $form->field($metaTagForm, '[' . $i . ']name')->textInput() ?>
    <?= $form->field($metaTagForm, '[' . $i . ']content')->textInput() ?>
    <?= 'Attrubutes:' ?>
    <?php foreach ($metaTagForm->attributes as $j => $attributeForm): ?>
        <?= $form->field($metaTagForm, '[' . $i . ']attributes[' . $j . '][key]')->textInput()->label('Key '.($j+1)) ?>
        <?= $form->field($metaTagForm, '[' . $i . ']attributes[' . $j . '][value]')->textInput()->label('Value '.($j+1)) ?>
    <?php endforeach; ?>
<?php endforeach; ?>

<?= $form->field($model, 'canonical')->textInput() ?>

<?= Html::submitButton('Save', ['class' => 'btn btn-primary']) ?>
<?php ActiveForm::end() ?>