<?php
use yii\helpers\Html;
?>
<div><strong>Created:</strong> <?= $page->getCreatedAt()->format('d-m-Y H:i:s') ?></div>
<div><strong>Route:</strong> <?= $page->getRoute() ?></div>
<div><strong>Route parameters:</strong><br>
    <?php foreach ($page->getRouteParams() as $key => $param) {
              echo $key.'='.$param.'<br>';
    } ?>
</div>
<div>
    <strong>Meta tags:</strong><br>
    <?php foreach ($page->getMetaTags() as $metaTag) {
        echo 'USE :'.Html::Encode($metaTag).'<br>';
        $attributes = '';
        foreach ($metaTag->getAttributes() as $key => $value) {
            $attributes .= ' '.$key.'="'.$value.'"';
        }
        echo 'OR :'.Html::Encode('<meta name="'.$metaTag->getAttributeName().'" content="'.$metaTag->getContent().'"'.$attributes.'>').'<br>';
    } ?>
</div>
<div><strong>Canonical tag:</strong> <?= Html::Encode($page->getCanonical()) ?></div>