<?php

/**
 * This file is part of the yii2-page-meta-data.
 *
 * Copyright 2020 Green Wave Palace Ltd. <jzwebstudio@gmail.com>.
 *
 * This source file is subject to the Commercial license that is bundled
 * with this source code in the file LICENSE.
 * @package yii2-page-meta-data
 */

namespace JzWebstudio\Yii2PageMetaData;

use Yii;

/**
 * yii2-page-meta-data module definition class
 *
 * @author Sergey Bakhtin <sergey.a.bahtin@gmail.com>
 */
class Module extends \yii\base\Module
{

    public $defaultRoute = 'page-meta';

    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'JzWebstudio\Yii2PageMetaData\Controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        \Yii::$container->setDefinitions([
            'RobotE13\PageMeta\Repositories\PageRepository' => 'JzWebstudio\Yii2PageMetaData\Repositories\Yii2DaoPageRepository',
        ]);
    }

}
