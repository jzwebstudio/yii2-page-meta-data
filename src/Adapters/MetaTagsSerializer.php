<?php
/**
 * This file is part of the yii2-page-meta-data.
 *
 * Copyright 2020 Green Wave Palace Ltd. <jzwebstudio@gmail.com>.
 *
 * This source file is subject to the Commercial license that is bundled
 * with this source code in the file LICENSE.
 * @package yii2-page-meta-data
 */

namespace JzWebstudio\Yii2PageMetaData\Adapters;

use RobotE13\PageMeta\Entities\MetaTag\{
    MetaTag,
    MetaTagsCollection
};

/**
 * MetaTagsSerializer
 *
 * @author Sergey Bakhtin <sergey.a.bahtin@gmail.com>
 */
class MetaTagsSerializer implements \JsonSerializable
{
    /**
     * @var MetaTagsCollection
     */
    private $metaTags;

    public function __construct(MetaTagsCollection $metaTags)
    {
        $this->metaTags = $metaTags;
    }

    public function jsonSerialize()
    {
        return array_map(function($item) {
            /* @var $item MetaTag */
            return [
                'name' => $item->getAttributeName(),
                'content' => $item->getContent(),
                'attributes' => json_encode($item->getAttributes())
            ];
        }, $this->metaTags->toArray());
    }

}